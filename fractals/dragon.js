/**
 * Created by olomako on 19.02.15.
 */

function DragonCurve(depth, step) {
    var path = new FractalBuilder()
        .init('FX')
        .addRule('X', 'X+YF+')
        .addRule('Y', '-FX-Y')
        .build(depth);
    var processor = new FractalProcessor()
        .when('F').move(step)
        .when('XY').doNothing()
        .when('-').rotate(-90)
        .when('+').rotate(90);

    Fractal.call(this, path, processor);
}
DragonCurve.prototype = new Fractal();