/**
 * Created by olomako on 19.02.15.
 */

function Plant(depth, step) {
    var path = new FractalBuilder()
        .init('X')
        .addRule('X', 'F-[[X]+X]+F[+FX]-X')
        .addRule('F', 'FF')
        .build(depth);
    var processor = new FractalProcessor()
        .when('F').move(step)
        .when('[').save()
        .when(']').load()
        .when('X').doNothing()
        .when('-').rotate(rand(30))
        .when('+').rotate(rand(-20));

    Fractal.call(this, path, processor);

    function rand(max) {
        return function () {
            return Math.random() * max;
        };

    }
}
Plant.prototype = new Fractal();