/**
 * Created by olomako on 19.02.15.
 */

function KochSnowflake(depth, step) {
    var path = new FractalBuilder()
        .init('F++F++F')
        .addRule('F', 'F-F++F-F')
        .build(depth);
    var processor = new FractalProcessor()
        .when('F').move(step)
        .when('-').rotate(-60)
        .when('+').rotate(60);

    Fractal.call(this, path, processor);
}
KochSnowflake.prototype = new Fractal();