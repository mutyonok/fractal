/**
 * Created by olomako on 19.02.15.
 */

function KochCurve(depth, step) {
    var path = new FractalBuilder()
        .init('F')
        .addRule('F', 'F+F-F-F+F')
        .build(depth);
    var processor = new FractalProcessor()
        .when('FG').move(step)
        .when('-').rotate(-90)
        .when('+').rotate(90);
    Fractal.call(this, path, processor);
}
KochCurve.prototype = new Fractal();