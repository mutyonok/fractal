/**
 * Created by olomako on 19.02.15.
 */

function SierpinskiTriangle(depth, step) {
    var path = new FractalBuilder()
        .init('F-G-G')
        .addRule('F', 'F-G+F+G-F')
        .addRule('G', 'GG')
        .build(depth);
    var processor = new FractalProcessor()
        .when('FG').move(step)
        .when('-').rotate(-120)
        .when('+').rotate(120);

    Fractal.call(this, path, processor);
}
SierpinskiTriangle.prototype = new Fractal();