/**
 * Created by olomako on 19.02.15.
 */

function SierpinskiTriangle2(depth, step) {
    var path = new FractalBuilder()
        .init('A')
        .addRule('A', 'B-A-B')
        .addRule('B', 'A+B+A')
        .build(depth);
    var processor = new FractalProcessor()
        .when('AB').move(step)
        .when('-').rotate(-60)
        .when('+').rotate(60);

    Fractal.call(this, path, processor);

}
SierpinskiTriangle2.prototype = new Fractal();