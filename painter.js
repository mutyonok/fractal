/**
 * Created by olomako on 19.02.15.
 */

function Painter(context, turtle) {
    var _x = 0, _y = 0, _angle = 0;

    this.startFrom = function(x, y, angle) {
        _x = x;
        turtle.x(x);
        _y = y;
        turtle.y(y);
        if (angle) {
            _angle = angle;
            turtle.rotate(angle);
        }
        return this;
    };
    this.drawPath = function drawPath(fractal) {
        context.beginPath();
        context.moveTo(turtle.x(), turtle.y());
        while(fractal.hasNext()) {
            var draw = fractal.process(turtle);

            if (draw) {
                context.lineTo(turtle.x(), turtle.y());
            } else {
                context.moveTo(turtle.x(), turtle.y());
            }
        }
        context.stroke();
    }
}