/**
 * Created by olomako on 19.02.15.
 */

/**
 * Represents turtle graphics technique.
 * Currently turtle can move, rotate and remember it's position
 * @constructor
 */
function Turtle() {
    const DEG_TO_RAD = Math.PI / 180;
    var x = 0, y = 0, angle = 0,
        savepoints = [];

    function toRad(angle) {
        return angle * DEG_TO_RAD;
    }

    this.move = function move(step) {
        step = step instanceof Function ? step() : step;
        x += step * Math.cos(angle);
        y -= step * Math.sin(angle);
    };
    this.rotate = function rotate(_angle) {
        angle += toRad(_angle instanceof Function ? _angle() : _angle);
    };
    this.save = function save() {
        savepoints.push({x: x, y: y, angle: angle});
    };
    this.rollback = function rollback() {
        var point = savepoints.pop();
        if (point) {
            x = point.x;
            y = point.y;
            angle = point.angle;
        }
    };

    this.x = function getX(value) {
        if (value) {
            x = value;
        }
        return x;
    };
    this.y = function getY(value) {
        if (value) {
            y = value;
        }
        return y;
    };
    this.angle = function (value) {
        angle = toRad(value);
    }
}