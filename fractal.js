/**
 * Created by olomako on 19.02.15.
 */

/**
 * Base class for all fractals.
 * @param {String[]} path represents fractal as a sequence of L-system commands
 * @param {FractalProcessor} processorFn callback function that translates L-system command to the turtle actions
 * @constructor
 */
function Fractal(path, processorFn) {
    var i = 0;
    this.hasNext = function() {
        return i < path.length;
    };

    this.next = function () {
        return path[i++];
    };

    this.reset = function() {
        i = 0;
    };

    this.process = function(turtle) {
        return processorFn.process(this.next(), turtle);
    }
}

/**
 * Builder of fractals based on L-system structure
 * @constructor
 */
function FractalBuilder() {
    var axiom = [], rules = {};

    /**
     * Defines axiom or initiator value of a fractal
     * @param {String} axiomString
     * @returns {FractalBuilder}
     */
    this.init = function (axiomString) {
        axiom = axiomString.split('');
        return this;
    };

    /**
     * Adds production rule for L-system variable
     * @param {String} fromSymbol character of the string to be transformed, predecessor in terms of L-systems
     * @param toString sequence of characters to be produced from predecessor, successor in terms of L-systems
     * @returns {FractalBuilder} returns self so methods of builder could be chained
     */
    this.addRule = function (fromSymbol, toString) {
        rules[fromSymbol] = toString.split('');
        return this;
    };
    /**
     * Recursively builds fractal sequence
     * @param depth depth of fractal
     * @returns {Array}
     */
    this.build = function (depth) {
        if (depth === 0) {
            return axiom;
        }
        var current = this.build(depth - 1);
        var next = [];
        for (var i = 0; i < current.length; i++) {
            var rule = rules[current[i]];
            if (rule) {
                Array.prototype.push.apply(next, rule);
            } else {
                next.push(current[i]);
            }
        }
        return next;
    }
}

/**
 * Describes turtle actions.
 * @param {FractalProcessor} processor
 * @param symbols commands to be translated into turtle actions
 */
function TurtleActions(processor, symbols) {
    function addRule(rule) {
        for (var i = 0; i < symbols.length; i++) {
            processor.rules[symbols[i]] = rule;
        }
    }

    return {
        /**
         * moves turtle
         * @param {Number|Function} step
         * @returns {FractalProcessor}
         */
        move: function (step) {
            addRule({action: 'move', args: [step], answer: true});
            return processor;
        },
        /**
         * rotates turtle
         * @param {Number|Function} angle use degree not radians
         * @returns {FractalProcessor}
         */
        rotate: function (angle) {
            addRule({action: 'rotate', args: [angle], answer: false});
            return processor;
        },
        /**
         * somtimes turtle don't have to do anything
         * @returns {FractalProcessor}
         */
        doNothing: function () {
            return processor;
        },
        /**
         * adds turtle checkpoint so it is possible to return to some previous turtle position
         * @returns {FractalProcessor}
         */
        save: function () {
            addRule({action: 'save', args: [], answer: false});
            return processor;
        },
        /**
         * loads previously saved checkpoint
         * @returns {FractalProcessor}
         */
        load: function () {
            addRule({action: 'rollback', args: [], answer: false});
            return processor;
        }
    };
}

/**
 * Builder for translation rules from L-system commands to the turtle actions.
 * @constructor
 */
function FractalProcessor() {
    this.rules = {};

    /**
     * Remembers commands that must be translated.
     * Usage: when('AB').move(10)  or when('-').rotate(-10);
     * @param {String} symbols terms that will be translated into turtle action
     * @returns TurtleActions turtle actions translator
     */
    this.when = function (symbols) {
        return new TurtleActions(this, symbols);
    };

    /**
     * Gives order to the turtle
     * @param symbol L-system command to process
     * @param turtle turtle to give an order to
     * @returns {boolean} true if line must be drawn for the last turtle action, false otherwise
     */
    this.process = function (symbol, turtle) {
        var rule = this.rules[symbol];
        if (rule && turtle[rule.action]) {
            turtle[rule.action].apply(turtle, rule.args);
            return rule.answer;
        }
    }
}